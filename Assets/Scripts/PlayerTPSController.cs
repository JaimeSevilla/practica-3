﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;
    public PlayableAsset cinematic;
    public Animator anim;
    public Camera brain;
    private PlayableDirector director;
    private bool played = false;
    public bool onInteractionZone { get; set; }
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
        director = GetComponent<PlayableDirector>();
    }

    void Update()
    {
        //Get input from player
        input.getInput();

        //Use jump as action button if
        // the character is inside an InteractionZone
        if(onInteractionZone && input.jump)
        {
            onInteractionInput.Invoke();
        }
        else
        //Apply input to character
        characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Cinematic 1" && !played)
        {
            played = true;
            director.playableAsset = cinematic;
            director.RebuildGraph();
            director.time = 0.0;
            director.Play();
        }
    }
}
